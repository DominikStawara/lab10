package ztwlab;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {
    public static void main(String[] args) {
        Document document = new Document();

        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream("Resume.pdf"));

            document.open();

            PdfPTable table = new PdfPTable(2); // 3 columns.

            PdfPCell cell1 = new PdfPCell(new Paragraph("First name"));
            PdfPCell cell2 = new PdfPCell(new Paragraph("Dominik"));
            PdfPCell cell3 = new PdfPCell(new Paragraph("Last name"));
            PdfPCell cell4 = new PdfPCell(new Paragraph("Stawara"));
            PdfPCell cell5 = new PdfPCell(new Paragraph("Profession"));
            PdfPCell cell6 = new PdfPCell(new Paragraph("Manager"));
            PdfPCell cell7 = new PdfPCell(new Paragraph("Education"));
            PdfPCell cell8 = new PdfPCell(new Paragraph("2017 - 2021 PWSZ Tarnów"));
            PdfPCell cell9 = new PdfPCell(new Paragraph("Summary"));
            PdfPCell cell0 = new PdfPCell(new Paragraph("Young, helpful and knowledgeable in many areas. Being a leader is my destiny."));

            cell1.setPadding(10);
            cell2.setPadding(10);
            cell3.setPadding(10);
            cell4.setPadding(10);
            cell5.setPadding(10);
            cell6.setPadding(10);
            cell7.setPadding(10);
            cell8.setPadding(10);
            cell9.setPadding(10);
            cell0.setPadding(10);

            Font font1 = new Font(Font.FontFamily.HELVETICA  , 25, Font.BOLD);

            Paragraph paragraph1 = new Paragraph();
            paragraph1.setSpacingAfter(80);
            paragraph1.setSpacingBefore(25);
            paragraph1.setAlignment(Element.ALIGN_CENTER);
            paragraph1.setIndentationLeft(50);
            paragraph1.setIndentationRight(50);

            Chunk chunk = new Chunk(
                    "Resume", font1);
            paragraph1.add(chunk);

            document.add(paragraph1);

            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.addCell(cell4);
            table.addCell(cell5);
            table.addCell(cell6);
            table.addCell(cell7);
            table.addCell(cell8);
            table.addCell(cell9);
            table.addCell(cell0);

            document.add(table);

            document.close();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}